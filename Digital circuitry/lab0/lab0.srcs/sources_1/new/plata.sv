`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09.03.2019 11:10:04
// Design Name: 
// Module Name: plata
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module plata(
    input   [7 : 0] btn,  // pushbuttons  
    output  [7 : 0] dig0, // digit 0
    output  [7 : 0] dig1, // digit 1
    output  [7 : 0] dig2, // digit 2
    output  [7 : 0] dig3,  // digit 3      
    output logic [8 : 0] led  // LEDs
    );
        
    logic [3:0] a;
    logic [3:0] b;    
    logic [4:0] sum;
    
    always_comb
        begin
            a = ~btn[3:0];
            b = ~btn[7:4];
            sum <= a + b;
            led[1:0] <= {2{a > b}};                    
            led[4:3] <= {2{a == b}};
            led[7:6] <= {2{a < b}};
        end  
              
    indicator show1(.in(a), .out(dig0));   
    indicator show2(.in(b), .out(dig1));    
    indicator show3(.in(sum[4]), .out(dig2));        
    indicator show4(.in(sum[3:0]), .out(dig3));       
endmodule
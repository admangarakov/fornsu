`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09.03.2019 11:10:59
// Design Name: 
// Module Name: indicator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module indicator(
    input [3:0] in,
    output logic [7:0] out
    );
    always_comb
        case(in)
            4'b0000: out <= 8'b00111111; 
            4'b0001: out <= 8'b00000110;
            4'b0010: out <= 8'b01011011;
            4'b0011: out <= 8'b01001111;
            4'b0100: out <= 8'b01100110;
            4'b0101: out <= 8'b01101101;
            4'b0110: out <= 8'b01111101;
            4'b0111: out <= 8'b00000111;
            4'b1000: out <= 8'b01111111;
            4'b1001: out <= 8'b01101111;    
            
            4'b1010: out <= 8'b01110111;
            4'b1011: out <= 8'b01111100;
            4'b1100: out <= 8'b00111001;
            4'b1101: out <= 8'b01011110;
            4'b1110: out <= 8'b01111001;
            4'b1111: out <= 8'b01110001;
        endcase
endmodule

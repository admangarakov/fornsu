`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.03.2019 11:26:46
// Design Name: 
// Module Name: gogo
// Project Name: 
// Target Devices: 
// Tool Versions:
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module gogo(
    input clk, 
    output logic led[7:0]
    );
    logic [24:0]count=0;
    logic go_right=1;
    logic [2:0]cntLight=0;
    always_ff @ (posedge clk)
    begin
        if(count++ >= 1_000_000)
        begin
            count=0;
            if(cntLight == 7)
                go_right=0;
            if(cntLight == 0)
                go_right=1;
            if(go_right)
            begin
                led[cntLight]=0;
                cntLight++;
                led[cntLight]=1;
            end
            else
            begin
                led[cntLight]=0;
                cntLight--;
                led[cntLight]=1;
            end
        end
    end;
endmodule

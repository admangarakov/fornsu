`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08.03.2019 20:18:07
// Design Name: 
// Module Name: muxer2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module muxer2(
    input [1:0] in,
    input sel,
    output out
    );
    
assign out = (!sel & in[0]) | (sel & in[1]);
endmodule

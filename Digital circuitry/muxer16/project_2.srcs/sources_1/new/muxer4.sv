`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.03.2019 11:04:52
// Design Name: 
// Module Name: muxer4
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module muxer4(
    input [3:0] in,
    input [1:0] sel,
    output out
    );
    
logic [1:0] tmp;
muxer2 m0(.in(in[1:0]), .sel(sel[0]), .out(tmp[0]));
muxer2 m1(.in(in[3:2]), .sel(sel[0]), .out(tmp[1]));
muxer2 m2(.in(tmp), .sel(sel[1]), .out(out));
endmodule

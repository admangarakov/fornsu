`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.03.2019 12:02:08
// Design Name: 
// Module Name: test16
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test16();

logic [15:0] in;
logic [3:0] sel;
logic out;

muxer16 DUT(.in(in), .sel(sel), .out(out));

initial begin

    sel = 4'b0000; in = 16'b000000000001; #10;
    sel = 4'b0000; in = 16'b111111111111; #10;
    
    sel = 4'b0001; in = 16'b000000000010; #10;
    sel = 4'b0001; in = 16'b111111111101; #10;
    
    sel = 4'b0010; in = 16'b000000000100; #10;
    sel = 4'b0010; in = 16'b111111111011; #10;
    
    sel = 4'b0011; in = 16'b000000001000; #10;
    sel = 4'b0011; in = 16'b111111110111; #10;
    
    sel = 4'b0100; in = 16'b000000010000; #10;
    sel = 4'b0100; in = 16'b111111101111; #10;
    
    sel = 4'b0101; in = 16'b000000100000; #10;
    sel = 4'b0101; in = 16'b111111011111; #10;
    
    sel = 4'b0110; in = 16'b000001000000; #10;
    sel = 4'b0110; in = 16'b111110111111; #10;
    
    sel = 4'b0111; in = 16'b000010000000; #10;
    sel = 4'b0111; in = 16'b111101111111; #10;
    
    sel = 4'b1000; in = 16'b000100000000; #10;
    sel = 4'b1000; in = 16'b111011111111; #10;
    
    sel = 4'b1001; in = 16'b001000000000; #10;
    sel = 4'b1001; in = 16'b110111111111; #10; 
    
$finish;
end;    
endmodule

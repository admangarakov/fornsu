`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.03.2019 10:56:06
// Design Name: 
// Module Name: counter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module counter(
    input clk,
    input [7:0] btn,
    output  [7 : 0] dig0,
    output  [7 : 0] dig1,
    output  [7 : 0] dig2,
    output  [7 : 0] dig3   
    );
    
    logic [3:0]dig[3:0]={0,0,0,0};    
    logic button[3:0] = {0,0,0,0};
    logic [24:0]count = 0;
    
    always_ff @ (posedge clk)
        begin        
            if (count++ >= 12_000_000)
                if (&btn[7:4])
                    begin                                
                        count = 0;
                        dig[0]++;   
                    end
            
            for(int i = 0; i < 4; i++)
                begin
                    if (!btn[i+1] && !button[i])
                            begin
                                button[i] = 1;
                                dig[i]++;
                            end
                    if (btn[i+1])
                        button[i] = 0;
                end
                    
            for (int i = 0; i < 3; i++)
                if (dig[i] >= 10) 
                    begin
                        dig[i] = 0;
                        dig[i+1]++;
                    end
                     
            if (dig[3] >= 10 || !btn[0])
                for(int i = 0; i < 4; i++)
                    dig[i] = 0;                              
        end
         
    indicator show1(.in(dig[0]), .out(dig3));        
    indicator show2(.in(dig[1]), .out(dig2)); 
    indicator show3(.in(dig[2]), .out(dig1)); 
    indicator show4(.in(dig[3]), .out(dig0)); 
    
endmodule

#define _CRT_SECURE_NO_WARNINGS
#include <malloc.h>
#include <stdio.h>

void swap(int *a, int *b) {
	int c = *a;
	*a = *b;
	*b = c;
}

void Heapify(int i, int size, int *arr) {
	int left = 2 * i - 1;
	int right = 2 * i;
	int largest = i - 1;
	if (left < size && arr[left] < arr[largest]) largest = left;
	if (right < size && arr[right] < arr[largest]) largest = right;
	if (largest != i - 1) {
		swap(&arr[i - 1], &arr[largest]);
		Heapify(largest + 1, size, arr);
	}
}

BuildHeap(int size, int *arr) {
	for (int i = size / 2; i > 0; i--) Heapify(i, size, arr);
}

int main() {
	int size;
	scanf("%d", &size);
	int *arr = (int*)malloc(sizeof(int) * size);
	for (int i = 0; i < size; i++) scanf("%d", &arr[i]);

	BuildHeap(size, arr);
	printf("heap: ");
	for (int i = 0; i < size; i++) printf("%d ", arr[i]);

	printf("\nmin = %d \nnew heap: ", arr[0]);
	swap(&arr[0], &arr[--size]);
	Heapify(1, size, arr);

	for (int i = 0; i < size; i++) printf("%d ", arr[i]);

	return 0;
}
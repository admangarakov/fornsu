#define _CRT_SECURE_NO_WARNINGS
#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct People {
	char Fname[50];
	char Sname[50];
	int score;
} people;

int cmp(const void *a, const void *b) {
	return ((people*)b)->score - ((people*)a)->score;
}

int main() {
	int n;
	scanf("%d", &n);
	people *arr = (people*)malloc(sizeof(people) * n);
	for (int i = 0; i < n; i++) {
		int buf;
		scanf("%s", &arr[i].Fname);
		scanf("%s", &arr[i].Sname);
		arr[i].score = 0;
		for (int j = 0; j < 3; j++) {
			scanf("%d", &buf);
			arr[i].score += buf;
		}
	}
	qsort(arr, n, sizeof(people), cmp);
	for (int i = 0; i < n; i++) {
		printf("%s %s\n", arr[i].Fname, arr[i].Sname);
	}
	return 0;
}
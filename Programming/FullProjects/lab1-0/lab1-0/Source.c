#define _CRT_SECURE_NO_WARNINGS
#define speedSize 256
#include <stdlib.h>
#include <stdio.h>

int main()
{
	FILE *input;
	input = fopen("in.txt", "r");
	if (input == NULL) {
		printf("Failed fopen");
		return 0;
	}
	int len = 0;
	char strBuf[17];
	for (;; len++)
	{
		fread(&strBuf[len], 1, 1, input);
		if (strBuf[len] == 10)
			break;
	}

	char  str[17];
	for (int i = 0; i < len - 1; i++)
		fread(&str[i], 1, 1, input);

	FILE *output;
	output = fopen("out.txt", "w");
	if (output == NULL) {
		printf("Failed fopen");
		return 0;
	}
	int cur = len - 1;
	char speed[speedSize];
	int speed2 = 0;
	for (int j = len - 1;; j++)
	{
		if (!(speed2 % speedSize)) {
			memset(speed, 0, speedSize);
			fread(&speed, speedSize, 1, input);
			speed2 = 0;
		}
		if (!(str[j % 16] = speed[speed2++]))
			break;
		if (j < cur)
			continue;
		int i = 0;
		for (; i < len; i++)
		{
			fprintf(output, "%d ", j - i + 1);
			if (strBuf[len - i - 1] != str[(j - i) % 16])
				break;
		}
		cur = j + len;
		for (int k = i; k < len; k++)
		{
			if (strBuf[len - k - 1] == str[j % 16])
			{
				cur = j + k;
				break;
			}
		}
	}
	fclose(input);
	fclose(output);
	return 0;
}

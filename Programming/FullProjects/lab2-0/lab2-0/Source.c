#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

void swap(int *a, int *b) {
	int c = *a;
	*a = *b;
	*b = c;
}

void sort(int *arr, int size) {
	if (size < 2) return;
	for (int i = 0; i < size; i++)
		for (int j = size - 1; j > i; j--)
			if (arr[j - 1] > arr[j]) swap(&arr[j - 1], &arr[j]);
}

int main() {
	int p;
	int arr[10];
	int badIn[10];
	for (int i = 0; i < 10; i++)badIn[i] = 0;
	char buf;
	scanf("%c", &buf);
	int size = 0;
	int bad = 0;
	for (; buf != '\n'; size++) {
		arr[size] = (int)buf - '0';
		if (badIn[arr[size]] || arr[size] > 9) {
			bad = 1;
		}
		else badIn[arr[size]] = 1;
		scanf("%c", &buf);
	}
	scanf("%d", &p);
	if (bad) {
		printf("bad input");
		return 0;
	}
	int count = 0;
	if (size < 2) return 0;
	while (p > count) {
		for (int i = size - 2; i >= 0; i--) {
			if (arr[i] >= arr[i + 1]) {
				if (i) continue;
				else return 0;
			}
			int minI = i, min = 10;
			for (int j = i + 1; j < size; j++) {
				if (arr[j] < arr[i] || arr[j] > min) continue;
				minI = j;
				min = arr[j];
			}
			if (minI == i) return 0;
			swap(&arr[i], &arr[minI]);
			sort(&arr[i + 1], size - i - 1);
			for (int k = 0; k < size; k++) {
				printf("%d", arr[k]);
			}
			printf("\n");
			count++;
			if (count == p) return 0;
			break;
		}
	}
	return 0;
}
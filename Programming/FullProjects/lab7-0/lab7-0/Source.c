#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
typedef struct Node Node;

struct Node {
	int val;
	Node *next;
};

void push(Node **top, int value) {
	Node *tmp = (Node*)malloc(sizeof(Node));
	tmp->next = *top;
	tmp->val = value;
	*top = tmp;
}

void printOut(Node *top) {
	printf("%d ", top->val);
	if (top->next) printOut(top->next);
}

void error(int i) {
	switch (i)
	{
	case 0:
		printf("bad number of vertices"); break;
	case 1:
		printf("bad number of edges"); break;
	case 2:
		printf("bad number of lines"); break;
	case 3:
		printf("bad vertex"); break;
	case 4:
		printf("impossible to sort"); break; 
	default:
		break;
	}
	exit(0);
}

void topSort(int **arr, int *used, Node **output, int v, int N) {
	if (arr[v]) {
		used[v] = -1;
		for (int i = 0; i < N; i++) {
			if (arr[v][i]) {
				if (used[i] == -1) error(4);
				if (!used[i]) topSort(arr, used, output, i, N);
			}
		}
	}
	used[v] = 1;
	push(output, v + 1);
};

int main() {
	Node *output = NULL;
	int N = -1, M = -1, a = -1, b = -1;
	scanf("%d", &N);
	if(scanf("%d", &M) == -1) error(2);
	if (N < 0 || N > 1000) error(0);
	if (M < 0 || M > N*(N+1)/2) error(1);

	int *used = (int*)malloc(N * sizeof(int));
	for (int i = 0; i < N; i++) used[i] = 0;
	int **arr = (int**)malloc(N * sizeof(int*));

	for (int i = 0; i < M; i++) {
		if(scanf("%d %d", &a, &b) == -1) error(2);
		if (a < 1 || a > N || b < 1 || b > N) error(3);
		if (!arr[a - 1]) { 
			arr[a - 1] = (int*)malloc(N * sizeof(int));
			for (int j = 0; j < N; j++)
				arr[a - 1][j] = 0;
		}
		arr[a - 1][b - 1] = 1;
	}

	for (int i = 0; i < N; i++)
		if (!used[i]) topSort(arr, used, &output, i, N);

	printOut(output);
	return 0;
}
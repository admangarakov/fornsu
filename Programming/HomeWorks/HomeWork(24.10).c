#include <stdio.h>

char * start;

char * strstr(char * str1, const char * str2) {
	for (int i = 0;; i++) {
		if (str1[i] == str2[0])
			for (int j = 1;; j++) {
				if (str1[i + j] != str2[j]) {
					if (str2[j] == '\0') return str1 + i;
					else break;
				}
			}
		if (str1[i] == '\0') break;
	}
	return NULL;
}


char * strtok(char * str, const char * delimiters) {
	if (str == NULL) {
		if (start == NULL)
			return NULL;
		else str = start;
	}
	int count = 0;
	for (int i = 0;; i++) {
		if (str[i] == '\0') {
			start = NULL;
			if (i > count)
				return str + count;
			else
				return NULL;
		}
		for (int j = 0; delimiters[j] != '\0'; j++) {
			if (str[i] == delimiters[j]) {
				str[i] = '\0';
				count++;
				if (i > count - 1) {
					start = str + i + 1;
					return str + count - 1;
				}
			}
		}
	}
	return NULL;
}

int main() {
	char a[] = "test1, test2,test3, .test4. ,test5.test6.............. ,test7.........";
	char * out = strtok(a, " ,.");
	while (out != NULL) {
		printf("%s\n", out);
		out = strtok(NULL, " ,.");
	}
	return 0;
}
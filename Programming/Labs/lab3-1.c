#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

void swap(int *a, int *b) {
	int c = *a;
	*a = *b;
	*b = c;
}

int partition(int *arr, int low, int high) {
	int p = low + 1;
	for (int i = low + 1; i <= high; i++)
		if (arr[i] <= arr[low])
			swap(&arr[i], &arr[p++]);
	swap(&arr[low], &arr[--p]);
	return p;
}

void qsort(int *arr, int low, int high) {
	if (low < high) {
		int mid = (low + high) / 2;
		swap(&arr[mid], &arr[low]);
		int p = partition(arr, low, high);
		qsort(arr, p + 1, high);
		qsort(arr, low, p - 1);
	}
}

int main() {
	int n;
	scanf("%d", &n);
	int *arr = (int*)malloc(sizeof(int) * n);
	if (arr == NULL) {
		printf("Failed malloc");
		return 0;
	}
	int flag = 0;
	for (int i = 0; i < n; i++) {
		scanf("%d", &arr[i]);
		if (i > 0 && arr[i] < arr[i - 1])flag = 1;
	}
	if (flag)
		qsort(arr, 0, n - 1);
	for (int i = 0; i < n; i++) {
		printf("%d ", arr[i]);
	}
	free(arr);
	return 0;
}
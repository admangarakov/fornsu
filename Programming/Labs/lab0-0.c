#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>

unsigned short toInt(char symbol) {
	unsigned short out = 0;
	out = (symbol >= 65 && symbol < 97) ? symbol - 55 : symbol - 48;
	if (symbol >= 97) {
		out = symbol - 87;
	}
	return out;
}

int main() {
	FILE *file1, *file2;
	file1 = fopen("in.txt", "r");
	if (file1 == NULL) { 
		printf("Failed fopen");
		return 0;
	}
	file2 = fopen("out.txt", "w"); 
	if (file2 == NULL) {
		printf("Failed fopen");
		return 0;
	}
	unsigned short counter = 0, badInput = 0, lengthBeforePoint = 0, lengthTotal = 0;
	unsigned long long leftPart = 0;
	unsigned int  b1 = 0, b2 = 0;
	double buffer = 0;
	char cBuf = ' ', x[50];
	fscanf(file1, "%d %d \n", &b1, &b2);

	while (1) {
		fscanf(file1, "%c", &cBuf);
		if (cBuf == '\n') {
			break;
		}
		if (cBuf == '.') {
			if (counter == 0 || lengthBeforePoint > 0) badInput = 1;
			lengthBeforePoint = counter;
		}
		else {
			x[counter] = cBuf;
			if (toInt(x[counter]) < 0 || toInt(x[counter]) >= b1) {
				badInput = 1;
			}
			counter++;
		}
	}
	fclose(file1);

	if (lengthBeforePoint == 0) {
		lengthBeforePoint = counter;
	}
	else {
		lengthTotal = counter;
		if (lengthTotal == lengthBeforePoint) badInput = 1;
	}

	if (b1 < 2 || b1 > 16 || b2 < 2 || b2 > 16) {
		badInput = 1;
	}

	for (int i = lengthBeforePoint - 1; i >= 0; i--) {
		leftPart += toInt(x[i]) * pow(b1, lengthBeforePoint - 1 - i);
	}

	for (int i = lengthBeforePoint; i < lengthTotal; i++) {
		buffer += toInt(x[i]) / pow(b1, (i - lengthBeforePoint + 1));
	}

	counter = 0;
	while (leftPart > 0) {
		x[counter] = leftPart % b2 + 48;
		if (x[counter] > 57) {
			x[counter] += 7;
		}
		leftPart /= b2;
		counter++;
	}

	if (counter > 0) {
		lengthBeforePoint = counter - 1;
	}
	else {
		x[0] = 48;
		lengthBeforePoint = 0;
		counter++;
	}

	for (int i = 0; i < 12; i++) {
		buffer *= b2;
		if ((int)buffer > 9) {
			x[counter] = (int)buffer + 55;
		}
		else {
			x[counter] = (int)buffer + 48;
		}
		buffer -= (int)buffer;
		counter++;
		if (buffer == 0) {
			break;
		}
	}
	lengthTotal = counter - 1;

	if (badInput) {
		fprintf(file2, "bad input");
	}
	else {
		for (int i = lengthBeforePoint; i >= 0; i--) {
			fprintf(file2, "%c", x[i]);
		}

		fprintf(file2, ".");

		for (int i = lengthBeforePoint + 1; i < counter; i++) {
			fprintf(file2, "%c", x[i]);
		}
	}
	fclose(file2);

	return 0;
}
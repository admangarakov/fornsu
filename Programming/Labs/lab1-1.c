#define _CRT_SECURE_NO_WARNINGS
#define speedSize 2048
#include <stdlib.h>
#include <stdio.h>

int main()
{
	FILE *input, *output;
	input = fopen("in.txt", "r");
	if (input == NULL) {
		printf("Failed fopen");
		return 0;
	}
	output = fopen("out.txt", "w");
	if (output == NULL) {
		printf("Failed fopen");
		return 0;
	}
	unsigned char pattern[17];
	unsigned char text[speedSize];
	unsigned int pow[] = { 1, 3, 9, 27, 81, 243, 729, 2187, 6561, 19683, 59049, 177147, 531441, 1594323, 4782969, 14348907, 43046721 };
	unsigned int len = 0, patternHash = 0, textHash = 0, firstHash, I = 0;
	for (;; len++)
	{
		fread(&pattern[len], 1, 1, input);
		if (pattern[len] == 10)
			break;
		patternHash += (pattern[len] % 3) * pow[len];
	}
	fprintf(output, "%u ", patternHash);
	memset(text, 0, speedSize);
	fread(&text, speedSize, 1, input);
	for (int j = 0; j < len; j++) {
		if (!text[j]) return 0;
		if (!j) firstHash = text[j] % 3;
		textHash += (text[j] % 3) * pow[j];
	}
	for (int i = len; text[I + len - 1] != 0; i++) {
		if (!(i % speedSize)) {
			I = 0;
			memset(text, 0, speedSize);
			fread(&text, speedSize, 1, input);
		}
		if (textHash == patternHash)
			for (int j = 0; j < len; j++) {
				fprintf(output, "%d ", i - len + j + 1);
				if (text[I + j] != pattern[j])break;
			}			
		textHash -= firstHash;
		firstHash = text[I + 1] % 3;
		textHash += (text[len + I++] % 3) * pow[len];
		textHash /= 3;
	}

}
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
typedef struct Node Node;

struct Node {
	int num;
	Node *next;
};

void push(Node **top, int value) {
	Node *tmp = (Node*)malloc(sizeof(Node));
	tmp->next = *top;
	tmp->num = value;
	*top = tmp;
}

int pop(Node **top, int isReturn, int isFree) {
	Node *out = *top;
	int value = out->num;
	if (isFree) {
		*top = (*top)->next;
		free(out);
	}
	if (isReturn)
		return value;
}

void erorr(int i) {
	if (i == 1) printf("syntax error\n");
	else printf("division by zero\n");
	exit(0);
}

void proc_op(Node **Num, char op) {
	if (!isOp(op)) erorr(1);
	if (*Num == NULL) erorr(1);
	int r = pop(Num, 1, 1);
	if (*Num == NULL) erorr(1);
	int l = pop(Num, 1, 1);
	if (op == '+') {
		push(Num, l + r);
		return;
	}
	if (op == '-'){
		push(Num, l - r);
		return;
	}
	if (op == '*'){
		push(Num, l * r);
		return;
	}
	if (op == '/') {
		if (r == 0)erorr(2);
		else push(Num, l / r);
		return;
	}
}

int prior(char op) {
	return op == '+' || op == '-' ? 1 : op == '*' || op == '/' ? 2 : -1;
}

int isDigit(char c) {
	return '0' <= c && c <= '9';
}

int isOp(char c) {
	return c == '+' || c == '-' || c == '*' || c == '/';
}

int main() {
	Node *Num = NULL;
	Node *Op = NULL;
	char CharBuf;
	int IntBuf = 0, flag = 0, flag1 = 0;
	while (1) {
		scanf("%c", &CharBuf);
		if (isDigit(CharBuf)) {
			flag = 1;
			IntBuf = 10 * IntBuf + CharBuf - '0';
		}
		else {			
			if (flag) { 
				push(&Num, IntBuf);
				flag1 = 0;
			}
			int I = 1;
			IntBuf = 0;
			flag = 0;
			if (CharBuf == 10) break;
			if (isOp(CharBuf)) {
				while (Op != NULL && prior(pop(&Op, 1, 0)) >= prior(CharBuf))
					proc_op(&Num, pop(&Op, 1, 1));
				push(&Op, CharBuf, 0);
				I = 0;
			}
			if (CharBuf == '(') { 
				push(&Op, '(');
				flag1 = 1;
				I = 0;
			}
			if (CharBuf == ')') {
				if (Op == NULL || (pop(&Op, 1, 0) == '(' && flag1)) erorr(1);
				while (Op != NULL && pop(&Op, 1, 0) != '(')
					proc_op(&Num, pop(&Op, 1, 1));
				if (Op == NULL) erorr(1);
				pop(&Op, 0, 1);
				I = 0;
			}
			if (I)erorr(I);
		}
	}
	while(Op != NULL)
		proc_op(&Num, pop(&Op, 1, 1));
	if (Num == NULL) erorr(1);
	printf("%d\n", pop(&Num, 1, 0));
	return 0;
}